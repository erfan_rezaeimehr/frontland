const colors = require('tailwindcss/colors')

module.exports = {
  theme: {
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      black: colors.black,
      white: colors.white,
      gray: colors.trueGray,
      red: colors.rose,
      blue: colors.blue,
      'blue-primary': '#0055FF',
      'blue-primary-light': '#3497FD',
    },
  },
}
